const isPrime = require('../utils/prime');

process.on('message', (message) => {
  const jsonResponse = isPrime(message.number);
  console.log(jsonResponse);
  process.send(jsonResponse);
  process.exit();
});
