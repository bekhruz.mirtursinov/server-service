// The class is a server for processing requests.
// transport is passed in the constructor, and clustering settings
// the server can work both in one process and spawn child processes to process requests.
// The task is to add the missing parts and refactor the existing code
//
// It doesn't matter if http / ws / tcp / or a simple socket, it is all isolated in the transport.
// The only thing that the request processing service knows is the type of transport connection, permanent or temporary
// and based on this creates the desired configuration. well, and also on what clustering mode was set
// In the final version, we expect to see the code in some version control system (github, gitlab) of your choice
// Examples of use for this or that transport
// It will be a plus if you use typescript and static typing in this case.

const { fork } = require('child_process');
const { join } = require('path');

class Service {
  constructor(options) {
    this.transport = options.transport;
    this.isClusterMode = options.isClusterMode;
    // if (this.isClusterMode) {
    //   this.clusterOptions = options.cluster;
    // }
  }

  async start() {
    await this.startTransport();
  }

  async startTransport() {
    //todo: логика запуска транспорта
    console.log('Start transport');
    this.transport.start();
  }

  async startCluster() {
    //todo: логика запуска дочерних процессов
    console.log('Start Cluster');
    return fork(join(__dirname, '/worker'));
  }
}

module.exports = Service;
