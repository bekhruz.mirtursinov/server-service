const http = require('http');
const pid = require.pid;

http.createServer((req, res)=>{
    for(let i=0; i<1e4;i++)
    res.end('Handled by process: '+pid);
}).listen(8080, ()=>console.log("Started at 8080 with pid: "+pid))