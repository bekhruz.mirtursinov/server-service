const fs = require('fs');

const server = require('https').createServer({
key:fs.readFileSync('./key.pem'),
cert:fs.readFileSync('./cert.pem')
});


server.on('request', (req,res)=>{
    res.writeHead(200);

    res.write("Hello world\n");
    
    // setTimeout(()=>{
    //     console.log("After 2seconds", )
    //     res.write("After 2 seconds\n");
    // }, 2000);
    res.end("end");

})


server.listen(8000, ()=>{console.log('https server is running on ',8000)});
 