// Server
const  dgram = require('dgram');

const server = dgram.createSocket('udp4');


const PORT = 3000;
const HOST = '127.0.0.1';


server.on('listening', ()=>{console.log('UDP Server listening')});
server.on('message', (msg, rinfo)=>{
    console.log(`${rinfo.address}:${rinfo.port} - ${msg}`);
})


server.bind(PORT);


// Client
const client = dgram.createSocket('udp4');
const msg = Buffer.from('for future');

client.send(msg,3, msg.length, PORT,(err)=>{
    if(err) throw err;

    console.log("UDP message sent");

    client.close();
}, 1000)