/*
redo without using the cluster module. 
Do through child_process and manually manage workers,
giving tasks to workers, balancing requests between workers, 
Examples of implementing various transports (not only http),
distribute them in different files

nеределать без использования модуля cluster. 
Сделать через child_process и ручное управление воркерами, 
передача задач воркерам, балансировка запросов между воркерами, 
примеры реализации различных транспортов (не только http), 
разнести по разным файлам

*/

const app = require('express')();
const Service = require('./services/service');

const service = new Service({
  transport: {
    isPermanentConnection: true,
    start: function () {
      app.listen(3000, () => console.log('Listening on 3000'));
    },
  },
});

app.get('/isprime', async (req, res) => {
  const childProcess = await service.startCluster();
  childProcess.send({ number: parseInt(req.query.number) });
  childProcess.on('message', (message) => res.send(message));
});

service.start();
