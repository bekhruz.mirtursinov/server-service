const {Readable} = require('stream');
const { setTimeout } = require('timers');
const inStream = new Readable({

    read(size){
        setTimeout(() => {
            if(this.currentCharCode>90){
                this.push(null);
                return;
            }    
            this.push(String.fromCharCode(this.currentCharCode++));
          
        }, 100);
        
    }
});

// inStream.push('ABDEFddkBJDSDLNSLDC');
// inStream.push(null);

inStream.currentCharCode=65;
inStream.pipe(process.stdout);
process.on('exit',()=> console.log('\n\n Current Code is '+inStream.currentCharCode));

process.stdout.on('error', ()=>{console.log("Stdout error")})