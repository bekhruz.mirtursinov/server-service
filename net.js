process.stdout.write('\u001B[2]');

const server = require('net').createServer();
var counter =0;
const sockets={}

function timeStamp(){
  const now = new Date();
  return `${now.getHours() }:${now.getMinutes()}`
}

server.on('connection', (socket) => {
  console.log('Client connected');
  socket.id=counter++;


  socket.write('Please enter your name: ');

  

  socket.on('data', (data) => {

    if(!sockets[socket.id]){
      socket.name = data.toString().trim();
      socket.write(`Welcome ${socket.name}\n`);
      sockets[socket.id]= socket;
      return;
    }
    Object.entries(sockets).forEach(([key,cs])=>{
      if(socket.id==key) return;
      cs.write(`[${socket.name}]<${timeStamp()}>: `);
      cs.write(data);
   
      })
    // console.log(`[${socket.id}]: `, data);
    });

  socket.on('end', () => {
    delete sockets[socket.id]
    console.log('Client is disconnected with Id: ', socket.id);
  });

  socket.setEncoding('utf-8');
});

server.listen(8000, () => {
  console.log('Server is on port 8000');
});
